import React from 'react';
import style from "./header.css";

import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';

import SideNavigation from "./SideNav/sidenav";

const Header = (props) => {

	const navBars = () => (
		<div className={style.bars}>
			<FontAwesomeIcon icon={faBars}
				onClick={props.onOpenNav}

				style={{
					color:"#dfdfdf",
					padding:"10px",
					cursor:"pointer"
				}}
			/>
		</div>
	)

	const logo = () => (
		<Link to="/" className={style.logo}>
			<img alt="Logo" src="/images/nba_logo.png"/>
		</Link>
	)

	return (
		<header className={style.header}>
			<SideNavigation {...props} />
			<div className={style.headerWrapper}>
				{navBars()}
				{logo()}
			</div>
		</header>
	)
}

export default Header;