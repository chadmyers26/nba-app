import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faNewspaper, faPlay, faSignInAlt, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

import style from "./sidenav.css";

const SideNavItems = () => {

	const items = [
		{
			type: style.option,
			icon: faHome,
			text: "Home",
			link: "/"
		},
		{
			type: style.option,
			icon: faNewspaper,
			text: "News",
			link: "/news"
		},
		{
			type: style.option,
			icon: faPlay,
			text: "Videos",
			link: "/videos"
		},
		{
			type: style.option,
			icon: faSignInAlt,
			text: "Sign In",
			link: "/sign-in"
		},
		{
			type: style.option,
			icon: faSignOutAlt,
			text: "Sign Out",
			link: "/sign-out"
		}
	]

	const showItems = () => {
		return items.map((item, i) => {
			return(
				<div key={i} className={item.type}>
					<Link to={item.link}>
						<FontAwesomeIcon icon={item.icon} />
						{item.text}
					</Link>
				</div>
			)
		})
	}

	return (
		<div>
			{showItems()}
		</div>
	)
}

export default SideNavItems;